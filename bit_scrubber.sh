# NOTE TO USER
# This is my first shell script (besides small scripts from tutorials)
# I wanted to make something useful for maintaining a clean environment on linux
# I sometimes find my ignorance to Linux (up until this class of course) leading me to having a messy Linux environment
# This script is aimed as a place to document a handful of useful commands I could use outside of the script as well as batch them all together in one command
# With that said, I am unsure how practical this may be in practice (requiring root access and all), please provide suggestions & further insights if you don't mind.
# I attempt to lay out my understanding in comments of what I THINK I am doing. Please critique aggessively :P

# Script constants
version=1.0

# FLAGS
do_prompt=false
print_logs=false
print_system_info=false
help=false #TODO: Unusued

# Future Development Idea: Have user provide this?
out_file_name="MySystemInfo"



# Function to provide an entire line of dashes to serve as a spacer from previous task
function format_line() {
	num_hyphens=$(tput cols)

	for (( i = 0; i < $num_hyphens; i++ ))
	do
		echo -n "-"
	done
	echo
}

# The user needs to run as a super user, this function ensures that is the case
function root_check() {
	# Was the script ran as a root user?
	if [ $EUID -gt 0 ]; then
		format_line
		echo "This script does system maintenance and thus needs root privileges." >/dev/tty
		echo "Please run as root." >/dev/tty
		format_line
		exit
	fi
}

# Function to print out the help screen for the script (given a -h or --help flag)
function script_help() {

	echo "This script does updates and system clean up and thus requires root to run."
	echo "Usage: bit_scrubber [OPTION]..."
	echo "Perform a variety of system maintenance tasks at the user's discretion."
	echo
	echo "  Short		  Long			  Description"
	echo "    -h		 --help		      Print this screen"
	echo "    -v		 --version	      Print the version"
	echo "    -p		 --prompt	      Prompt before executing any command"
	echo "    -l		 --logs		      Output to log file"
	echo "    -s		 --sysout	      Create system details output file"

	# We only want to provide the help screen since the user may run this without root access and for conciseness.
	exit
}

# Grab any and all flags and adjust the script environment to reflect the set options
for arg in $@
do
	first_char=${arg:0:1} # Grab the first character
	second_char=${arg:1:1}
	if [[ ($first_char = -) && ($second_char != -) ]]; then
		flags=${arg:1} # Grab all but first character (ignore -)

		for (( i=0; i<${#flags}; i++ ))
		do
			flag=${flags:$i:1}
			case $flag in
				h) #help
					script_help
					;;
				v) #version
					echo "Version $version"
					;;
				p) #prompt
					do_prompt=true
					;;
				l) #logs
					print_logs=true
					;;
				s) #system info out file
					print_system_info=true
					;;
				*) # All other cases
					echo "'$flag' is not an accepted option. -h for help."
					exit
					;;

			esac
		done
	fi
done

# Since we handled the options (and possible help option) we are now ready to enforce the root requirement
# I wanted a user to be able to print out help without needing root access.
root_check

# Transfer the output (stdout) only to a separate file to serve as a log
if [ $print_logs = true ]; then
	log_file="My_Logs"
	echo "Printing output to log file: $log_file"
	#exec 3>&1 4>&2 >$log_file 2>&1
	exec 1> My_Logs
	#exec 2> My_Errors
fi

# A helper function that handles all commands being executed to ensure they succeed
function validate_command() {
	# Eval allows me to execute a command that has had arguments concatenated to it
	eval $@
	# Was the command successfully executed?
	if [ $? -ne 0 ]; then
		format_line
		echo "$@ command failed!" | tee /dev/tty
		echo "Terminating scipt!" | tee /dev/tty
		format_line
		exit
	fi
	echo
}

# Function to handle pretty spacing information output
# This allows for my loading icons to be on the same line
function inform() {
	echo -n "$@  " >/dev/tty

	# Only add newline if we are not expecting run symbols to show
	# These are the spining /|\ .. symbols
	# If we don't use them (i.e. print all output to terminal)
	#  then we will want the next output to be on a new line
	if [ $print_logs = false ]; then
		echo
	fi
}

# A function to handle all the cases where we need to prompt the user for permission to run a command
function prompt() {
	answer=y
	if [ $do_prompt = true ]; then
		echo >/dev/tty
		read -p "Run $1 (y/n)? " answer #1>/dev/tty
	fi
}

# Function to handle the execution of any command
# Also provides a loading feature (uses /-\|)
function execute() {
	format_line
	
	# If we print to a log then we want to run the command in the background
	if [ $print_logs = true ]; then
		eval $@ &
	else
		eval $@
	fi

	# Grab the pid of the last executed command
	pid=$!

	i=1
	sp="/-\|"
	# Print out the load icons while we wait for the last command to finish
	# This is to allow the user to know that the terminal is still active (not frozen)
	# We only do this if the output (stdout) is directed to a file
	if [ $print_logs = true ]; then
		# Give user some feedback that the process is still running
		while kill -0 $pid 2> /dev/null;
		do
			# SOURCE: https:stackoverflow.com/questions/238073/how-to-add-a-progress-bar-to-a-shell-script
			# \b is use to get rid of last character
			# Then take 1 character starting at position i from sp
			# i%${#sp} is ensuring our i value does not exceed size of sp
			printf "\b${sp:i++%${#sp}:1}" >/dev/tty
			# Sleep to provide a slower loading effect
			sleep 1

			# If we have an exit status of non-zero then something went wrong
			# Perform an early termination of this loop to handle this error below
			[ $? -ne 0 ] && break
		done
		# Remove the last loading character (keeps the screen clean of loading debris)
		printf "\b " > /dev/tty
		# add an extra line in for convenience 
		echo >/dev/tty
	fi
		
	# Command failed, inform the user and kill the script
	if [ $? -ne 0 ]; then
		echo "$@ command failed!" | tee /dev/tty
		echo "Terminating scipt!" | tee /dev/tty
		exit
	fi

	# Inform the user that the command finished successfully
	format_line
	echo "Command completed!" | tee /dev/tty
	format_line
	echo
}


# Now we are ready to prompt commands to the user

# Handle the apt-get update and apt-get dist-upgrade commands
# Prompt if the user set that flag
prompt "apt-get update && apt-get dist-upgrade"

# Inform the user of the current command being executed. Execute that command
if [[ $answer =~ ^[Yy] ]]; then
	inform "Updating..."
	execute "yes | apt-get update"
	inform "Upgrading..."
	execute "yes | apt-get dist-upgrade"
fi

# Handle the apt-get autoclean, apt-get autoremove, and apt-get clean commands
# Prompt if the user set that flag
prompt "apt-get autoclean && apt-get autoremove && apt-get clean"

# Inform user of what we are doing and execute the commands
if [[ $answer =~ ^[Yy] ]]; then
	inform "Removing unnecessary packages..."
	execute "yes | apt-get autoremove"
fi


# Function to print out a statistics file of the system
function print_out_file() {
	file_name=$1
	# Future Development Idea: Make these script arguments?
	top_proc=5
	top_list=5
	file_size_low=100M
	file_size_high=1G
	days_unmodified=90
	days_unaccessed=90
	date=$(date)
	
	# The below is a sequence of various system information commands to populate our system information out file
	# Validate command is a function defined above to ensure successful execution occurred

	# Inform the user of the file containing the system information
	echo "Printing system information to $file_name" >/dev/tty
	exec 1> $file_name
	format_line
	echo "SYSTEM INFORMATION FOR $USER AS OF: $date"
	format_line
	echo -n "Uptime: "; validate_command uptime;
	format_line
	echo
	
	# Print out information for the CPU
	format_line
	echo 'CPU information...'
	format_line
	validate_command lscpu
	
	# Print out information about the current USB connections
	format_line
	echo 'USB connections...'
	format_line
	validate_command lsusb
	
	# Print out the amount of free space available
	format_line
	echo 'Free space available...'
	format_line
	validate_command "df -h"
	
	# Print out some number of processes that are using the most memory
	format_line
	echo "Top $top_proc processes using memory by %"
	format_line
	validate_command "ps -eo %mem,%cpu,comm --sort=-%mem | head -n $(($top_proc+1))"
	
	# Print out some number of files that are the top largest in the filesystem
	format_line
	echo "Top $top_list largest files in filesystem..."
	format_line
	validate_command "du -k /var/log | sort -n | tail -$top_list"
	
	# Print out all files whose size is within some range
	format_line
	echo "Files with size of $file_size_low to $file_size_high..."
	format_line
	validate_command "find . -type f -size +$file_size_low -size -$file_size_high -ls"
	
	# Print out some number of files that have been most recently modified
	format_line
	echo "$top_list most recent file modificatioons..."
	format_line
	validate_command "ls -larth /var/log | tail -$top_list"
	
	# Print out names of all files that haven't been modified for some amount of days
	format_line
	echo "Files that are $days_unmodified days unMODIFIED..."
	format_line
	validate_command "find /var/log -mtime +$days_unmodified -ls"
	
	# Print out names of all files that haven't been accessed for some amount of days
	format_line
	echo "File that are $days_unaccessed days unACCESSED..."
	format_line
	validate_command "find /var/log -atime +$days_unaccessed -ls"
	
	# Print out names of all empty files
	format_line
	echo "Files that are empty..."
	format_line
	validate_command "find . -type f -size 0b -ls"
	
	format_line
	# Get the size of the syslog file
	size=$(stat -c "%s" /var/log/syslog)
	# Get how many syslog files there are (some may be rotated)
	num_rotated_logs=$(find /var/log/ -name "syslog*" | wc -l)
	# We only care about the number of rotated logs for this variable, get rid of 1 (the original syslog)
	((num_rotated_logs--))
	echo >/dev/tty
	echo "There are $size bytes in /var/log/syslog."
	echo "There are $num_rotated_logs rotated log files."
	format_line
	
	echo "END"
	format_line
}

# If we have been printing to a log, output a newline to the terminal for some padding
if [ $print_logs = true ]; then
		#exec 1>&3 2>&4
		echo >/dev/tty
fi

# If the user set the option to print the system info file, then print it
if [ $print_system_info = true ]; then
	print_out_file "$out_file_name"
fi

